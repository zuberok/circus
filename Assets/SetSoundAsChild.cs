﻿using UnityEngine;
using System.Collections;

public class SetSoundAsChild : MonoBehaviour {

	void Awake()
	{
		Sound.Controller.transform.parent = transform;
		Sound.Controller.transform.localPosition = Vector3.zero;
		Sound.PlayMusic("GameProcess",0);
	}
	void OnDestroy()
	{
		Sound.Controller.transform.parent = null;
	}
}
