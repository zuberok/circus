﻿using UnityEngine;
using System.Collections;

public class OpenBtns : MonoBehaviour {
	public bool isOpen=false;
	private TweenPosition anim;
	public Vector3 end;
	public Vector3 begin;
	public GameObject btnRate;
	void Start()
	{
		anim = gameObject.transform.parent.GetComponent<TweenPosition> ();
		begin = anim.from;
		end = anim.to;
	}
	IEnumerator OnClick() {
	
		

	if (isOpen == false) {

						isOpen = true;

			     		anim.from=begin;
						anim.to=end;
						anim.enabled = true;
						anim.Reset ();
						anim.PlayForward ();
			if (btnRate.GetComponent<OpenRaitings>().isOpen==false){
				yield return new WaitForSeconds (1f);
				btnRate.GetComponent<OpenRaitings>().StopCoroutine("AnimationPlay");
				btnRate.GetComponent<OpenRaitings>().StartCoroutine("AnimationPlay");	
				yield return new WaitForSeconds(1.8f);
				
				
			}

				} else {
			isOpen=false;

			if (btnRate.GetComponent<OpenRaitings>().isOpen==true){
				btnRate.GetComponent<OpenRaitings>().StopCoroutine("AnimationPlay");
				btnRate.GetComponent<OpenRaitings>().StartCoroutine("AnimationPlay");	
				yield return new WaitForSeconds(1.8f);


			}

			anim.from=end;
			anim.to=begin;
			anim.Reset ();
			anim.PlayForward ();
				}
	}
}
