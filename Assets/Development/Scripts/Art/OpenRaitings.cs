using UnityEngine;
using System.Collections;

public class OpenRaitings : MonoBehaviour {
	public GameObject Rate;
	public GameObject Garmon;
	public GameObject RateList;
	public GameObject listMenu;
	public GameObject RateUI;
	public GameObject Info;
	private tk2dSpriteAnimator anim;
	public bool isOpen=false;


      void	OnClick () {
	
		StartCoroutine("AnimationPlay");
}
	public IEnumerator AnimationPlay()
	{
		if (isOpen == false) {
			isOpen = true;

			NGUITools.SetActive (Rate, true);

			anim = Garmon.GetComponent<tk2dSpriteAnimator> ();
			anim.StopAndResetFrame ();
			
			anim.Play ("open");
			yield return new WaitForSeconds (0.6f);
			NGUITools.SetActive (RateList, true);
			
			anim = listMenu.GetComponent<tk2dSpriteAnimator> ();
			anim.StopAndResetFrame ();
			anim.Play ("RateOpen");
			yield return new WaitForSeconds (0.7f);
			
			NGUITools.SetActive (RateUI, true);
			NGUITools.SetActive (RateList, false);
			
			NGUITools.SetActive (Info, true);
			yield return new WaitForSeconds (0.5f);
			
		} else {

			NGUITools.SetActive (Info, false);
			yield return new WaitForSeconds (0.5f);
			NGUITools.SetActive (RateList, true);
			
			NGUITools.SetActive (RateUI, false);
			
			anim = listMenu.GetComponent<tk2dSpriteAnimator> ();
			anim.StopAndResetFrame ();
			anim.Play ("RateClose");
			yield return new WaitForSeconds (0.7f);
			anim = Garmon.GetComponent<tk2dSpriteAnimator> ();
			anim.StopAndResetFrame ();
			NGUITools.SetActive (RateList, false);	                                
			
			anim.Play ("Close");
			yield return new WaitForSeconds (0.6f);
			NGUITools.SetActive (Rate,false);
			isOpen = false;

			
			
		}

	}



}
