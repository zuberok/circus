﻿using UnityEngine;
using System.Collections;

public class OnClickScale : MonoBehaviour {
	public GameObject btn;
	public GameObject Ball;
	public GameObject [] Anothers;
	bool first=true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	 void OnClick () {
				if (first ==true) {
			first=false;
			NGUITools.SetActive(btn,false);
						foreach (var col in Anothers) {
								col.GetComponent<TweenAlpha> ().enabled = true;
								col.collider.enabled = false;

						}

						gameObject.GetComponent<TweenScale> ().enabled = true;
						gameObject.GetComponent<TweenScale> ().Reset ();
						gameObject.GetComponent<TweenScale> ().Play ();

						NGUITools.SetActive (Ball, true);


						gameObject.GetComponent<TweenPosition> ().enabled = true;
						gameObject.GetComponent<TweenPosition> ().Reset ();
						gameObject.GetComponent<TweenPosition> ().Play ();

				}
		}
}
