﻿using UnityEngine;
using System.Collections;

public class LionScore : MonoBehaviour {
	public int score;
	void OnTriggerExit(Collider col){
		if(col.CompareTag("Player"))
		{
			GameController.TempScore += score;
			GUIController.Reward(transform, score);
		}
	}
}
