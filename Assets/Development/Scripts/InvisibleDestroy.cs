﻿using UnityEngine;
using System.Collections;

public class InvisibleDestroy : MonoBehaviour {
	bool sawn;
	void OnBecameVisible(){
		sawn = true;
	}
	void OnBecameInvisible(){
		if(sawn)
		{
			if(transform.parent.parent != null && transform.parent.parent.name != "Ring(Clone)")
			{
				Destroy(transform.parent.gameObject);
			}
			else
			{
				Destroy(transform.parent.parent.gameObject);
			}
		}
	}
}
