﻿using UnityEngine;
using System.Collections;

public class SetPanelStatus : MonoBehaviour {

	public GameObject panel;

	public bool enabled;

	
	void OnClick()
	{
		NGUITools.SetActive(panel, enabled);
	}
}
