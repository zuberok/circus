using UnityEngine;
using System;
//using System.Collections;

public class Highscore
{
	static int Capacity = 5;

	static string Key = "HIGHSCORE";

	public static int GetHighScoreWithNumber(int n)
	{
		InitializeKey();
		string[] _temp = PlayerPrefs.GetString("HIGHSCORE").Split();		
		try{
			
			return int.Parse(_temp[_temp.Length - n]);
		}
		catch
		{
			Debug.LogError("Invalid requested value is out of range: " + n + ". Maximal index is: " + _temp.Length + ". -1 was returned");
			return -1;
		}

	}
	public static int[] GetCurrentScore()
	{
		InitializeKey();
		string[] _temp = PlayerPrefs.GetString(Key).Split();
		int[] result = new int[_temp.Length];
		for(int i = 0; i < _temp.Length; ++i)
		{
			result[i] = int.Parse(_temp[i]);
		}
		return result;
	}
	public static bool WriteResultIfInTop(int score)
	{
		GUIController.FinishScore(score); // не здесь должно быть
		InitializeKey();
		int[] scores = GetCurrentScore();
		PlayerPrefs.DeleteKey("PROGRESS");
		if(score > scores[0])
		{
			if(score > scores[scores.Length - 1])
			{
				GUIController.NewHighScore = true;
			}
			scores[0] = score;
			Array.Sort(scores);

			string _temp = scores[0].ToString();
			for(int i = 1; i < Capacity; ++i)
			{
				Debug.Log(i);
				_temp += " " + scores[i].ToString();
			}
			PlayerPrefs.SetString(Key, _temp);
			return true;
		}
		return false;

	}
	public static void InitializeKey()
	{
		if(!PlayerPrefs.HasKey(Key))
		{
			string result = "0";
			for(int i = 1; i < Capacity; ++i)
			{
				result += " 0";
			}
			PlayerPrefs.SetString(Key, result);
		}
	}
}