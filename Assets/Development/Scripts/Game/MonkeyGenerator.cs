﻿using UnityEngine;
using System.Collections;

public class MonkeyGenerator : MonoBehaviour 
{
	public float pivotDistance;
	public float timer;
	public float height;
	public int generationsBetweenSM;
	public GameObject superMonkey;
	public GameObject[] monkeys;
	Transform previousMonkeys;

	public float maxDistance;

	private int generationsLeft;
	private Vector3 instVector;
	void Start()
	{
		generationsLeft = generationsBetweenSM;
		InitMonkey();
	}
	void InitMonkey()
	{
		instVector = previousMonkeys == null ? (transform.position.x + 55f) * Vector3.right + height * Vector3.up : (previousMonkeys.position.x + pivotDistance) * Vector3.right + height * Vector3.up;
		if( instVector.x - transform.position.x < maxDistance )
		{
			if(instVector.x - transform.position.x < 80f)
			{
				instVector.x += 50f;
			}	

			if(generationsLeft > 0)
			{
				generationsLeft--;
				if(instVector.x < LevelBuilder.Length - 50f){
					previousMonkeys = ((GameObject)Instantiate(monkeys[Random.Range(0, monkeys.Length)], instVector, Quaternion.identity)).transform;	
				}
			}
			else
			{
				generationsLeft = generationsBetweenSM;
				previousMonkeys = ((GameObject)Instantiate(superMonkey, instVector, Quaternion.identity)).transform;
			}

		}
		if(instVector.x < GetComponent<LevelBuilder>().levelLength * 5 - 20f)
		{
			Invoke("InitMonkey", timer);
		}
	}
}
