﻿using UnityEngine;
using System.Collections;

public class JumperGenerator : MonoBehaviour 
{
	public float minDistanceBetweenJumpers;
	public float maxDistanceBetweenJumpers;
	public float distanceBeforeFinish;

	public float firstJumperX;

	public GameObject[] jumpers;
	public float jumpersHeight;
	

	void Awake()
	{
		Vector3 nextVector = Vector3.right * firstJumperX + Vector3.up * jumpersHeight;
		while(nextVector.x < GetComponent<LevelBuilder>().levelLength * 5f - distanceBeforeFinish)
		{
			Instantiate(jumpers[Random.Range(0, jumpers.Length)], nextVector, Quaternion.identity);
			nextVector += Vector3.right * Random.Range(minDistanceBetweenJumpers, maxDistanceBetweenJumpers);
		}
	}
}
