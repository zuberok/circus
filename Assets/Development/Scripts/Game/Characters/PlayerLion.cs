﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerLion : MonoBehaviour 
{
    public List<Transform> backs = new List<Transform>();
    public GameObject[] buttons;
    public float walkingSpeed;
    public float walkingY;
    public float jumpingForce;
    public float maxBackLength;
    public bool finished = false;
    
    public bool moveLeft;
    public bool moveRight;
    bool auto;
    bool prevml;
    bool prevmr;
    tk2dSpriteAnimator playerAnim;// = transform.Find("PlayerLion")
    Transform myTransform;
    public FollowPlayer cam;
    public tk2dSpriteAnimator myAnim;
    
    public bool alive = true;
    bool burned = false;
    float maxX;
    public void AutoPilot()
    {
        if(!auto)
        {
            moveRight = true;
            foreach (GameObject go in buttons)
            {
                go.active = false;
            }

            auto = true;
            jumpingForce *= 1.2f;
            Invoke("Jump", 2.0f);             
            Invoke("FreeCam", 2.0f);
            Debug.Log("AUTOPILOT");        
                 
        } 
    }    
    void FreeCam()     
    {
        backs.Clear();
        cam.Unchain();
    }     
    void Awake()     
    {
        maxX = maxBackLength;
        myTransform = transform;
        maxX = myTransform.position.x;
        myAnim = transform.Find("Sprite").GetComponent<tk2dSpriteAnimator>();
        playerAnim = transform.Find("PlayerLion").transform.Find("anim").GetComponent<tk2dSpriteAnimator>();
    }     
    void Update()     
    {
        if(moveLeft && alive && !finished) 
        { 
            MoveLeft();
        }
        if(moveRight && !finished)  
        {
            MoveRight();
        }              
        if(Input.GetKeyDown(KeyCode.UpArrow)) 
        {
            Jump();
        }         
        if(Input.GetKeyDown(KeyCode.LeftArrow) && !finished && !auto)
        {
            moveLeft = true;
        }
        if(Input.GetKeyDown(KeyCode.RightArrow) && !finished && !auto)  
        {
            moveRight = true;
        }
        if(Input.GetKeyUp(KeyCode.LeftArrow) && !auto) 
        {
            moveLeft = false;
            Sound.Stop(1);
            myAnim.Play("Idle");
        }
        if(Input.GetKeyUp(KeyCode.RightArrow) && !auto)  
        {
            moveRight = false;
            Sound.Stop(1);
            myAnim.Play("Idle");
        }
        if(transform.position.y < walkingY && rigidbody.velocity.y != 0)         
        {
            rigidbody.velocity = Vector3.zero;
            transform.position += Vector3.up * (walkingY - transform.position.y);
        }
        if((moveRight || moveLeft) && !Sound.IsPlaying(1, "LionWalking") && !myAnim.IsPlaying("jump"))             
        {
           Sound.PlaySound("LionWalking", 1);
           
           myAnim.Play(moveRight ? "Run" :"RunBack");
        }
        if(((!prevml && moveLeft) || (!prevmr && moveRight)) && !Sound.IsPlaying(1, "LionJump"))         
        {
           Sound.PlaySound("LionWalking", 1);
        }
        if(!myAnim.IsPlaying("jump"))
        {
            if(((prevmr && !moveRight) || (prevml && !moveLeft))) 
            {
                
                myAnim.Play("Idle");
            }
            else if((!prevmr && moveRight) || (!prevml && moveLeft)) 
            {
                myAnim.Play(moveRight ? "Run": "RunBack");
            }
        }   
        prevmr = moveRight;
        prevml = moveLeft;

        if(!alive && !moveRight)         
        {
            collider.isTrigger = true;
            moveRight = true;
        }
    }
    public void Jump()
    {        
        if(transform.position.y <= walkingY && alive)
        {
            rigidbody.velocity = Vector3.zero;

            rigidbody.AddForce(Vector3.up * jumpingForce);
            myAnim.Play("jump");
            Sound.PlaySound("LionJump",1);
        }
    }
    public void MoveLeft()
    {
        if (maxX - myTransform.position.x < maxBackLength && alive)
        {
            myTransform.position += Vector3.left * walkingSpeed * Time.deltaTime;
            if(transform.Find("PlayerLion") != null)
            {
                foreach (Transform t in backs)
                {
                    t.position = Vector3.MoveTowards(t.position, t.position + Vector3.left, walkingSpeed * Time.deltaTime * 0.5f);
                }
            }
        }
    }
    public void MoveRight()
    {
        
        myTransform.position += Vector3.right * walkingSpeed * Time.deltaTime;
        if(transform.Find("PlayerLion") != null)
        {
            foreach (Transform t in backs)
            {
                t.position = Vector3.MoveTowards(t.position, t.position + Vector3.right, walkingSpeed * Time.deltaTime * 0.5f);
            }
        }
        if(myTransform.position.x > maxX)
        {
            maxX = myTransform.position.x;
        }
    }
    void Die(string anim)
    {
        collider.enabled = false;
        foreach (GameObject go in buttons)
        {
            go.active = false;
        }
        Rigidbody _temp = playerAnim.transform.parent.rigidbody;
        _temp.useGravity = true;
        _temp.isKinematic = false;
        _temp.collider.isTrigger = true;
        transform.Find("PlayerLion").transform.parent = null;
        rigidbody.detectCollisions = false;
       // rigidbody.isKinematic = true;
        alive = false;
        collider.isTrigger = true;
        //rigidbody.AddForce(200 * Vector3.down);
        playerAnim.Play(anim);
        GameController.FinishLevel(false);
        myAnim.Play("Run");
    }
    void OnTriggerEnter(Collider col)
    {
        switch (col.tag)
        {
            case "Barrier":
                if(alive)
                {
                    Die("Fall");
                }
                break;
            case "Finish":
                if(!finished && alive)
                {
                    finished = true;
                    collider.isTrigger = false;
                    GameController.FinishLevel(true);
                    moveRight = moveLeft = false;
                    alive = false;
                    playerAnim.Play("WinLion");
                    transform.Find("PlayerLion/BoomWin").active = true;
                }
                break;
            case "Fire":
                bool _temp = transform.position.x - playerAnim.transform.position.x < 5f && !burned;
                if(alive && !burned)
                {
                    Die("FireDead");
                    Sound.PlaySound("LionBurn", 2);
                }                   
                else if (_temp)
                {
                    Debug.Log(_temp);
                    playerAnim.Play("FireDead");
                    Sound.PlaySound("LionBurn", 2);
                    Debug.Log(transform.position.x - playerAnim.transform.position.x);
                }
                burned = true;
                break;
            default:
                if(!myAnim.IsPlaying("jump"))
                {
                    if(!moveRight && !moveLeft)
                    {
                        myAnim.Play("Idle");                
                    }
                    else
                    {
                        myAnim.Play(moveRight ? "Run" : "RunBack");
                        Debug.LogWarning("UNTAGGED COLLIDER ANIMATION");
                    }
                }
                break;
        }
    }
}