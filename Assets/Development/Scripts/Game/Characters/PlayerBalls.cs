using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBalls : MonoBehaviour {
	public int scorePerBall;
	public List<Transform> backs = new List<Transform>();
	public Vector3 distanceFromBall;
	public Vector3 jumpingVector; // куда прыгать
	public Ball currentBall; 
	public static Ball CurrentBall;
	public float jumpingSpeed;
	public float backSpeed;
	float prevX;
	Transform myTransform;
	public float walkingY;
	public static bool alive = true;
	tk2dSpriteAnimator myAnim;
	public float maxBackLength;
	public bool find = true;
	Ball.Direction prevdir;
	static PlayerBalls me;
	bool grounded = false;
	public FollowPlayer cam;
	public GameObject[] buttons;
	public bool auto;
	public bool nextRight = false;
	public bool nextLeft = false;
	void Awake()
	{
		me = this;
		currentBall.Activate(transform.position.x);
		CurrentBall = currentBall;
		currentBall.Use();
		alive = true;
		myTransform = transform;
		myAnim = transform.Find("Sprite").GetComponent<tk2dSpriteAnimator>();
	}
	public void AutoPilot()
    {
		if(!auto)
        {
            if(currentBall != null)
            {
            	currentBall.currentDirection = Ball.Direction.Right;
            }
            foreach (GameObject go in buttons)
            {
                go.active = false;
            }

            auto = true;
            
            Invoke("Jump", 3.3f);
            Invoke("FreeCam", 3.2f);
            Debug.Log("AUTOPILOT");
        }    
    }
	public void Jump()
	{
		if(currentBall != null)
		{
			currentBall.Drop();
			myAnim.Play("Jump");
			Sound.PlaySound("BallJump",1);
			currentBall.currentDirection = Ball.Direction.Left;
			currentBall.collider.enabled = false;
			//currentBall.Invoke("Liquidate", 2f);
			currentBall = null;
			rigidbody.isKinematic = false;
			rigidbody.AddForce(jumpingVector * jumpingSpeed);
		}
	}
	void FreeCam()
    {
        backs.Clear();
        cam.Unchain();
    }
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Ball") && currentBall == null && alive && find)
		{
			
			GameController.TempScore += scorePerBall;
			GUIController.Reward(transform, scorePerBall);
			CurrentBall = currentBall = col.GetComponent<Ball>();
			currentBall.Use();
			currentBall.Activate(transform.position.x);			
			rigidbody.isKinematic = true;
			myAnim.Play("Run");
			myAnim.Stop();
			if(auto || nextRight)
			{
				col.GetComponent<Ball>().currentDirection = Ball.Direction.Right;
				nextRight = false;
			}
			else if(nextLeft)
			{
				col.GetComponent<Ball>().currentDirection = Ball.Direction.Left;
				nextLeft = false;
			}

		}
		else if(col.CompareTag("Finish"))
		{
			Debug.Log("Finish");
			rigidbody.isKinematic = true;
			collider.isTrigger = false;
			transform.Find("BoomWin").active = true;
			myAnim.Play("Win");
		}
	}
	public void Damage()
	{
		foreach (GameObject go in buttons)
        {
            go.active = false;
        }
		alive = false;
		currentBall = null;
		if(transform.position.y < walkingY)
		{
			rigidbody.isKinematic = true;
			rigidbody.useGravity = false;
		}
		else
		{
			rigidbody.isKinematic = false;
		}
		if(GameController.FinishLevel(false))
		{
			myAnim.Play("Fall");
		}
		else
		{
			myAnim.Play("GameOver");
		}
	}
	void Update()
	{
		foreach (Transform t in backs)
		{
			t.position = t.position + (prevX - transform.position.x) * Vector3.right * Time.deltaTime * backSpeed;
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			Jump();
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			TurnLeft();
			Sound.PlaySound("BallRolling", 2);
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			TurnRight();
			Sound.PlaySound("BallRolling", 2);
		}
		else if (Input.GetKeyUp(KeyCode.RightArrow) || (Input.GetKeyUp(KeyCode.LeftArrow)))	
		{	
			if (currentBall != null && !auto)
			{
				currentBall.currentDirection = Ball.Direction.Stay;	
			}
			if(Input.GetKeyUp(KeyCode.RightArrow))
			{
				nextRight = false;
			}
			else if(Input.GetKeyUp(KeyCode.RightArrow))
			{
				nextLeft = false;
			}
			Sound.Stop(2);
		}
		else if (currentBall != null)
		{
			myTransform.position = currentBall.transform.position + distanceFromBall;
		}
		if (currentBall != null && prevdir != currentBall.currentDirection)	
		{
			if (currentBall.currentDirection != Ball.Direction.Stay)	
			{
				myAnim.Play("Run");
			}
			else 
			{
				myAnim.Stop();
			}
		}

		prevX = transform.position.x;
		if (myTransform.position.y < walkingY)
		{
			if (alive) 
			{
				Damage();
			}
			else if(!grounded)
			{
				grounded = true;
				rigidbody.useGravity = false;
				rigidbody.velocity = Vector3.zero;
			}
		}
		if (currentBall != null) {	prevdir = currentBall.currentDirection;	}
		if (currentBall != null && myTransform.position.x < Ball.maxX - maxBackLength)
		{
			currentBall.transform.position += (Ball.maxX - maxBackLength - currentBall.transform.position.x) * 2 * Vector3.right;
			currentBall.currentDirection = Ball.Direction.Stay;
			Sound.Stop(1);
		}
	}
	public void TurnLeft()
	{
		if(currentBall != null)
		{
			currentBall.currentDirection = Ball.Direction.Left;
		}
		nextLeft = true;
	}
	public void TurnRight()
	{
		if(currentBall != null)
		{
			currentBall.currentDirection = Ball.Direction.Right;
		}
		nextRight = true;
	}
	public static void Kill()
	{
		me.Damage();
	}

}
