﻿using UnityEngine;
using System.Collections;

public class Pony : MonoBehaviour {

	
	public float height;
	public Transform player;
	public float speedWithoutRider;
	void Start()
	{
		Sound.PlaySound("HorseWalking", 4);
	}
	void Update()
	{
		if(player.GetComponent<Rider>().alive)
		{
			transform.position = (player.position.x + 1f) * Vector3.right + height * Vector3.up;
		}
		else
		{
			transform.position += Vector3.right * Time.deltaTime * speedWithoutRider;
		}
	}
	//DontDestroyOnLoad(Object target);
}
