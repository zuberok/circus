﻿using UnityEngine;
using System.Collections;

public class BottomBorder : MonoBehaviour {
	public float borderY;
	void Update () {
		if(transform.parent == null && transform.position.y < borderY)
		{
			rigidbody.isKinematic = true;
			rigidbody.useGravity = false;
		}
	}
}
