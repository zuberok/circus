﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rider : MonoBehaviour {

	public float minSpeed;
	public float maxSpeed;
	public Transform pony;
	public float walkingY;
	public float jumpingForce;
	public List<Transform> backs = new List<Transform>();
	public bool auto = false;
	public FollowPlayer cam;
	
	public GameObject[] buttons;
	public tk2dSpriteAnimator myAnim;
	public bool jumping;
	bool inAir;
	public bool alive;
	public bool slow
	{
		set {currentSpeed = value ? minSpeed : maxSpeed; }
		get {return currentSpeed == minSpeed;}
	}
	public float currentSpeed;
	float prevY;
	void Awake()
	{
		alive = true;
		myAnim = transform.Find("Anim").GetComponent<tk2dSpriteAnimator>();
		inAir = false;
		slow = false;
		
	}
	public void AutoPilot()
    {
    	if(!auto)
    	{
    		foreach (GameObject go in buttons)
	        {
	            go.active = false;
	        }
    		auto = true; currentSpeed = maxSpeed;
	        myAnim.Play("Sit1");
	        Invoke("FinishJump", 1.4f);
	        Invoke("FreeCam", 1.8f);
	        Debug.Log("AUTOPILOT");
    	}
    }
     void FreeCam()
    {
        backs.Clear();
        cam.Unchain();
    }
    void FinishJump()
    {
    	Jump(jumpingForce, false);
    }
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			slow = true;
		}
		else if(Input.GetKeyUp(KeyCode.LeftArrow))
		{
			slow = false;
		}
		if(Input.GetKeyDown(KeyCode.UpArrow) && !inAir)
		{
			Jump(jumpingForce, false);
		}
		Vector3 _temp = transform.position;
		if(transform.position.y < walkingY)
		{
			rigidbody.useGravity = false;
			rigidbody.velocity = Vector3.zero;
			_temp.y = walkingY;
		}
		if(alive) 
		{
			_temp += Vector3.right * currentSpeed * Time.deltaTime;
			foreach(Transform t in backs)
			{
				t.position += Vector3.left * currentSpeed * Time.deltaTime / 20f;
			}
		}
		if(prevY >= walkingY && transform.position.y < walkingY && alive)
		{
			myAnim.Play("Sit1");
		}
		transform.position = _temp;
	}
	public void Jump(float force, bool fromJumper)
	{
		if(!fromJumper && alive && rigidbody.velocity.y == 0f && transform.position.y <= walkingY)
		{
			myAnim.Play("Jump");
			rigidbody.useGravity = true;
			rigidbody.velocity = Vector3.zero;
			Sound.PlaySound("RiderHorseJump", 1);
			rigidbody.AddForce(Vector3.up * force);
			Debug.LogWarning(fromJumper);
		}
		else if (fromJumper && alive)
		{
			Sound.PlaySound("RiderJumperJump", 3);
			myAnim.Play("Jump");
			rigidbody.velocity = Vector3.zero;
			rigidbody.AddForce(Vector3.up * force);
		}
			

	}
	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Barrier") && alive)
		{
			Die();
		}
		else if (col.CompareTag("Finish"))
		{	
			rigidbody.isKinematic = true;
			alive = false;
			myAnim.Play("Win");
			transform.Find("BoomWin").active = true;
		}
	}
	void PlayDieAnim()
	{
		myAnim.Play("Fall");
	}
	void Die()
	{
		foreach (GameObject go in buttons)
        {
            go.active = false;
        }
		walkingY -= 4f;
		Invoke("PlayDieAnim", 0.2f);
		alive = false;
		rigidbody.isKinematic = false;
		rigidbody.useGravity = true;
		GameController.FinishLevel(false);
	}
}
