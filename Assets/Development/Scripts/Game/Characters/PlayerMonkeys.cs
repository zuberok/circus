using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerMonkeys : MonoBehaviour 
{
    public GameObject[] buttons;
    public float walkingSpeed;
    static float WalkingSpeed;
    public float walkingY;
    public float jumpingForce;
    public float secondJumpForce;
    public List<Transform> backs;
    static Transform myTransform;
    public bool jumping = true;
    public bool Right = false;
    public bool Left = false;    
    static float JumpingForce;
    float maxDistance;
    public float maxBackLength;
    public bool alive = true;
    public tk2dSpriteAnimator myAnim;
    bool firstJump;
    bool idled;
    public bool auto = false;
    public FollowPlayer cam;
    void Awake()
    {
        backs = new List<Transform>();
        WalkingSpeed = walkingSpeed;
        myTransform = transform.parent;
        maxDistance = transform.position.x;
        myAnim = GetComponent<tk2dSpriteAnimator>();
    }
    public void AutoPilot()
    {

        foreach (GameObject go in buttons)
        {
            go.active = false;
        }
        auto = true; Right = true;
        myAnim.Play("Run");
        jumpingForce *= 1.2f;
        Invoke("Jump", 3.2f);
       //    Invoke("Jump",3.2f);
        Invoke("FreeCam", 2.8f);
        //Debug.Log("AUTOPILOT");
    }
    void FreeCam()
    {
        backs.Clear();
        cam.Unchain();
    }
    void Update()
    {
        if(Right && alive)
        {
            if(!Sound.IsPlaying(1, "MonkeyPlayerWalk") && !Sound.IsPlaying(1, "MonkeyPlayerJump") && !Sound.IsPlaying(1, "CoinTake")) {Sound.PlaySound("MonkeyPlayerWalk", 1);}
            MoveRight();
        }

        else if(Left && alive)
        {
            if(!Sound.IsPlaying(1, "MonkeyPlayerWalk") && !Sound.IsPlaying(1, "CoinTake")) {Sound.PlaySound("MonkeyPlayerWalk", 1);}
            if(maxDistance - myTransform.position.x < maxBackLength)
            {
                MoveLeft();
            }
        }
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            Right = true;
            myAnim.Play("Run");
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Left = true;
            myAnim.Play("Run");
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow) && !auto)
        {
            Right = false;
             myAnim.Play("Idle");
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Left = false;
            myAnim.Play("Idle");
        }
        if((Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow)) && alive && !auto)
        {
            myAnim.Play("Idle");
            Sound.Stop(1);
        }
        if(Input.GetKeyDown(KeyCode.UpArrow) && !auto)
        {
            Jump();
        }
        if(myTransform.position.x > maxDistance)
        {
            maxDistance = myTransform.position.x;
        }
        if(!Right && !Left && myAnim.IsPlaying("Idle") && idled)
        {
            myAnim.Play("Idle");
        }
        if(myTransform.position.y < walkingY)
        {
            if(!alive)
            {
                transform.parent.position += (walkingY - myTransform.position.y) * Vector3.up;
                transform.parent.rigidbody.isKinematic = true;
                myAnim.Play("Fall");
            }
            else if(transform.parent.rigidbody.velocity.y < 0 && !idled)
            {
                if(Right || Left)
                {
                   //// Debug.Log("RUN ANUMATOPM");
                    myAnim.Play("Run");
                }
                else if(!Right && !Left && transform.parent.rigidbody.velocity.y < -1f)
                {
                   //// Debug.LogError(transform.parent.rigidbody.velocity.y);
                    myAnim.Play("Idle");
                    idled = true;
                }
            }
        }

    }
    public void Jump()
    {
        if(myTransform.position.y < walkingY + 0.1f && alive)
        {
            myTransform.rigidbody.AddForce(Vector3.up * jumpingForce);
            myAnim.Play("Jump");
            //Debug.LogWarning("JUMP ANIMATION");
            firstJump = true;
            Sound.PlaySound("MonkeyPlayerJump", 1);
            idled = false;
        }
        else if(alive && firstJump)
        {
            firstJump = false;
            myTransform.rigidbody.AddForce(Vector3.up * secondJumpForce);
            myAnim.Play("Jump");
            //Debug.LogWarning("JUMP ANIMATION");
            Sound.PlaySound("MonkeyPlayerJump", 1);
            idled = false;
        }
    }
    void MoveLeft()
    {
        Vector3 _temp = myTransform.position + Vector3.left * WalkingSpeed * Time.deltaTime;
        foreach (Transform t in backs)
        {
            t.position = Vector3.MoveTowards(t.position, t.position + Vector3.left, WalkingSpeed * Time.deltaTime * 0.5f);
        }
        myTransform.position = _temp;
    }
    void MoveRight()
    {
        Vector3 _temp = myTransform.position +  Vector3.right * WalkingSpeed * Time.deltaTime;
        foreach (Transform t in backs)
        {
            t.position = Vector3.MoveTowards(t.position, t.position + Vector3.right, WalkingSpeed * Time.deltaTime * 0.5f);
        }
        myTransform.position = _temp;
    }
    void OnTriggerEnter(Collider col){
        if ((col.CompareTag("Barrier") || col.CompareTag("Monkey")) && alive)
        {
            Die();
        }
        else if (col.CompareTag("Finish"))
        {
            transform.parent.transform.Find("BoomWin").gameObject.active = true;
            Left = Right = false;
            alive = false;
            transform.parent.rigidbody.isKinematic = true;
            GameController.FinishLevel(true);
            myAnim.Play("Win");
            //Debug.LogWarning("WIN");
        }
    }
    void Die()
    {
        foreach(GameObject go in buttons)
        {
            go.active = false;
        }
        myTransform.rigidbody.AddForce((Vector3.up + Vector3.left) * jumpingForce * 0.25f);
        cam.Unchain();
        alive = false;
        walkingY -= 10f;
        transform.parent.GetComponent<Bonus>().Disable();
        GameController.FinishLevel(false);
        transform.parent.rigidbody.velocity = Vector3.zero;
        transform.parent.collider.isTrigger = true;
    }

}