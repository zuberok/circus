﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Acrobat : MonoBehaviour {
	public GameObject[] buttons;
	public Vector2 durationRange;
	public int scorePerRope;
	public float ropeCooldown = 1f;
	public Transform currentRope;
	public Vector3 jumpingVector;
	public List<Transform> backs;
	public float backMovingSpeed;	
	public float aliveY;
	public float ropeMoveDelta;
	public bool alive = true;
	public int ropeCount = 1;
	float prevX;
	tk2dSpriteAnimator myAnim;
	Transform prevRope;
	public Transform[] ropes;
	Queue<Transform> qRopes;
	public Vector3 jumperVector;
	bool finished = false;
	float ropeTimer;
	public float distanceBeforeFinish;
	bool lastRope;
	void Start () 
	{
		prevRope = currentRope;
		qRopes = new Queue<Transform>(ropes);
		myAnim = transform.Find("Sprite").GetComponent<tk2dSpriteAnimator>();
		transform.position = currentRope.position;
		transform.parent = currentRope;
		rigidbody.useGravity = false;		
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			if(currentRope != null)
			{
				Jump(jumpingVector);
				
			}
		}
		ropeTimer += Time.deltaTime;
		foreach(Transform t in backs)
		{
			t.position += Vector3.left * (transform.position.x - prevX) * backMovingSpeed;
		}
		transform.eulerAngles = Vector3.zero;
		prevX = transform.position.x;
		if(transform.position.y < aliveY && alive)
		{
			Die();
		}
	}
	void Die()
	{
		foreach (GameObject go in buttons)
        {
            go.active = false;
        }
		alive = false;
		GameController.FinishLevel(false);
		rigidbody.useGravity = false;
		rigidbody.velocity = Vector3.zero;
		myAnim.Play("Fall");
			
	}
	public void Jump(Vector3 vector)
	{
		if (alive && currentRope != null)
		{
			Sound.PlaySound("RopeJump",1);
			ropeTimer = 0f;
			myAnim.Play("Jump");
			transform.parent = null;
			
			rigidbody.isKinematic = false;
			rigidbody.AddForce(vector);
			rigidbody.useGravity = true;

			currentRope = null;
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Handle") && currentRope == null && !finished && (col.transform != prevRope || ropeTimer > ropeCooldown))
		{
			HoldRope(col);
		}
		else if(col.CompareTag("Finish"))
		{
			finished = true;
			GameController.FinishLevel(true);
			myAnim.Play("Win");
			transform.Find("BoomWin").active = true;
		}
		else if(col.CompareTag("Jumper") && alive)
		{		
			JumperJump();
			Sound.PlaySound("TrampolineJump",1);
			col.GetComponent<tk2dSpriteAnimator>().Play();
		}
	}
	void JumperJump()
	{
		myAnim.Play("Jump");
		rigidbody.velocity = Vector3.zero;
		rigidbody.AddForce(jumperVector);
		currentRope = null;
	}
	void HoldRope(Collider col)
	{
		ropeCount++;
		GameController.TempScore += scorePerRope;
		GUIController.Reward(transform, scorePerRope);
		if(ropeCount != 2 && prevRope != col.transform)
		{	
			if(qRopes.Peek().position.x + ropeMoveDelta < GetComponent<LevelBuilder>().levelLength * 5f - distanceBeforeFinish)
			{
				qRopes.Peek().position += ropeMoveDelta * Vector3.right;
			}
			else if (!lastRope)
			{
				qRopes.Peek().position = (GetComponent<LevelBuilder>().levelLength * 5f - distanceBeforeFinish) * Vector3.right + Vector3.up * 10f;
				lastRope = true;
			}
			qRopes.Peek().transform.Find("Rope").GetComponent<TweenRotation>().duration = Random.Range(durationRange.x, durationRange.y);
			qRopes.Peek().transform.Find("Jump1").active = true;
			qRopes.Enqueue(qRopes.Peek());
			qRopes.Dequeue();
		}
		prevRope = col.transform;
		myAnim.Stop();
		transform.parent = currentRope = col.transform;
		transform.localPosition = Vector3.zero;
		rigidbody.isKinematic = true;
		rigidbody.useGravity = false;
		rigidbody.Sleep();
		myAnim.Play("Rope");
	}
}
