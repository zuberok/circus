﻿using UnityEngine;
using System.Collections;

public class DisplayUIReward : MonoBehaviour 
{
	public GameObject Reward;
	public Camera gameCamera;
	public Camera uiCamera;
	public bool monkey;

	public void RewardDisplay(Transform Target, int rwrd)
	{
		Vector3 tempPos;
		GameObject tempUI= NGUITools.AddChild(HUDRoot.go,Reward);



		if (monkey == true) {
				
		 tempPos = new Vector3 (transform.position.x-5,Target.position.y+5, transform.position.z);
		}
		else
		 tempPos = new Vector3 (transform.position.x-5,transform.position.y, transform.position.z);

		Vector3 pos = gameCamera.WorldToViewportPoint(tempPos);
		tempUI.transform.position = uiCamera.ViewportToWorldPoint(pos);
		tempUI.transform.position= new Vector3(tempUI.transform.position.x,tempUI.transform.position.y, -1);
		tempUI.transform.localScale = Reward.transform.localScale;
		tempUI.GetComponent<UILabel>().text="+"+rwrd.ToString();
		tempUI.SendMessage("Destroy");
	}
}
