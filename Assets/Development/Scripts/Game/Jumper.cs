﻿using UnityEngine;
using System.Collections;

public class Jumper : MonoBehaviour {
	public int score;
	public float force;
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Player"))
		{
			Rider r = col.GetComponent<Rider>();
			if(r.alive && col.isTrigger)
			{
				//Debug.LogError(gameObject + " " + transform.parent);
				GameController.TempScore += score;
				GUIController.Reward(transform, score); 
				r.jumping = true;
				transform.Find("sprite").GetComponent<tk2dSpriteAnimator>().Play();
				r.Jump(force * 2, true);
			}
		}
	}
}
