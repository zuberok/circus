﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Finish : MonoBehaviour 
{
	public tk2dSpriteAnimator anim;
	public string winAnimationName;
	bool used = false;
	public MonoBehaviour player;
	public delegate void AutoPilot();
	public AutoPilot ap;
	void OnBecameVisible()
	{
		if(ap != null){ap();}
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player" && !used)
		{
			Debug.Log(col.gameObject);
			used = true;
			if	(col.rigidbody != null && !col.rigidbody.isKinematic) {col.rigidbody.velocity = Vector3.zero;}
			else if (col.rigidbody != null){ col.rigidbody.isKinematic = true; }
			GameController.FinishLevel(true);
			col.isTrigger = false;
		}
	}
}
