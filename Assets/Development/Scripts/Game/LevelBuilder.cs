﻿using UnityEngine;
using System.Collections;

public class LevelBuilder : MonoBehaviour {

	public enum Level
	{
		Monkeys,
		Lion,
		Pony,
		Balls,
		Ropes,
	}
	public static float Length;
	public Level level;
	public float finishY;
	public float tableY;
	public float floorHeight;
	public float backY;
	public GameObject finishPrefab;
	public GameObject tablePrefab;
	public float tableStep;
	public float levelLength;
	public GameObject backPrefab;
	public GameObject floorPrefab;
	void Awake()
	{
		Length = levelLength * 5F;
		Transform par = (new GameObject("$Level")).transform;
		
		GameObject _temp;

		for(int i = 1; i * tableStep < levelLength; ++i)
		{
			_temp = ((GameObject)Instantiate(tablePrefab, Vector3.right * i * tableStep * 5 + Vector3.up * tableY, Quaternion.identity));
			_temp.transform.Find("TextMesh").GetComponent<tk2dTextMesh>().text = ((levelLength - i * tableStep)).ToString();
			_temp.transform.parent = par;
		}
		for(int i = -1; i < 3; ++i)
		{
			_temp = (GameObject)Instantiate(floorPrefab, i * 55f * Vector3.right + Vector3.up * floorHeight, Quaternion.identity);
			_temp.transform.parent = par;
		}
		GameObject finish = (GameObject)Instantiate(finishPrefab, Vector3.right * levelLength * 5 + Vector3.up * finishY, Quaternion.identity);
		for(int i = -1; i < 2; ++i)		{
			_temp = (GameObject)Instantiate(backPrefab, i * Vector3.right * 54f + Vector3.up * backY, Quaternion.identity);
			
			switch (level)
			{
				case Level.Monkeys:
					transform.Find("Sprite").GetComponent<PlayerMonkeys>().backs.Add(_temp.transform);
					finish.transform.Find("Sprite").GetComponent<Finish>().ap = transform.Find("Sprite").GetComponent<PlayerMonkeys>().AutoPilot;
					break;
				case Level.Lion:
					GetComponent<PlayerLion>().backs.Add(_temp.transform);
					finish.transform.Find("Sprite").GetComponent<Finish>().ap = GetComponent<PlayerLion>().AutoPilot;
					break;
				case Level.Pony:
					GetComponent<Rider>().backs.Add(_temp.transform);
					finish.transform.Find("Sprite").GetComponent<Finish>().ap = GetComponent<Rider>().AutoPilot;
					break;
				case Level.Balls:
					GetComponent<PlayerBalls>().backs.Add(_temp.transform);
					finish.transform.Find("Sprite").GetComponent<Finish>().ap = GetComponent<PlayerBalls>().AutoPilot;
					break;
				case Level.Ropes:
					GetComponent<Acrobat>().backs.Add(_temp.transform);
					finish.transform.Find("Sprite").GetComponent<Finish>().ap = null;
					break;
			}
			_temp.transform.parent = par;
		}
		
	}
}
