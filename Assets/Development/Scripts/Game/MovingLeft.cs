﻿using UnityEngine;
using System.Collections;

public class MovingLeft : MonoBehaviour {

	public float speed;
	void Update()
	{
		transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.left, speed * Time.deltaTime);
	}
	void OnBecameInvisible()
	{
		Destroy(gameObject);
	}
}
