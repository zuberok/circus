﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {
	public Transform player;
	public float deltaX;
	public float distanceFromImage;
	delegate void Behaviour();
	Behaviour current;
	void Follow()
	{
		transform.position = (player.position.x + deltaX) * Vector3.right + Vector3.back * distanceFromImage;
	}
	void Awake()
	{
		current = Follow;
	}
	void Update () 
	{
		current();		
	}
	public void Unchain()
	{
		current = delegate(){};
		Debug.Log("CAMERA UNCHAINED");
	}
}
