﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{	
	public static int LevelChoosingScene = 1;
	public static int CoinsForResurrect = -1;
	public static int Lifes = 3;
	public static int Score;
	public static int CurrentLevel;
	static int tempScore;
	public static int TempScore
	{
		get{ return tempScore;}
		set{ 
			tempScore = value; 
			if(GUIController.GUI != null)
			{
				GUIController.GUI.scoreInfo.text = (Score + value).ToString();
			}
			else
			{
				Debug.Log("NO GUIController.GUI");
			}
		}
	}
	static int score;
	
	public static void GoToLevelChoosing()
	{
		Application.LoadLevel(LevelChoosingScene);
	}

	public static void LoadLevel(int numberOfLevel)
	{
		if(Lifes == 0) { Lifes = 3; }
		CurrentLevel = numberOfLevel;
		Application.LoadLevel(numberOfLevel);
	}

	public static bool FinishLevel(bool success)
	{
		Sound.Stop(0);
		Sound.Stop(1);
		Sound.Stop(2);
		if(success)
		{
			Sound.PlayMusic("Win", 3);
			TempScore += Bonus.bonus.currentScore;
			Bonus.tempValue = -1;
			Bonus.bonus.currentScore = 0;
			Score += TempScore;
			Lifes = 3;
			SaveScore();
			GUIController.Win(Score);
			TempScore = 0;
			return true;
		}
		else
		{
			Lifes--;
			if(Lifes > 0)
			{
				Sound.PlayMusic("Lose", 3);
				TempScore = 0;
				Bonus.tempValue = Bonus.bonus.currentScore;
				SaveScore();
				GUIController.Lose();
				return true;
			}
			else
			{
				ClearScore();
				
				
				Highscore.WriteResultIfInTop(Score);
				GUIController.GameOver();
				return false;
			}
		}
	}
	static void ClearScore()
	{
		PlayerPrefs.DeleteKey("CURRENT_SCORE");
	}
	public static void LoadScore()
	{
		Score = PlayerPrefs.GetInt("CURRENT_SCORE");
	}
	public static void SaveScore()
	{
		Debug.Log("SCORE SAVED");
		PlayerPrefs.SetInt("CURRENT_SCORE", Score);
	}
}