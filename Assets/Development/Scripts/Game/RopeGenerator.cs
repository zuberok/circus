﻿using UnityEngine;
using System.Collections;

public class RopeGenerator : MonoBehaviour {
	public GameObject ropePrefab;
	public float ropeAttidude;

	public void NewRope(float x)
	{
		Instantiate(ropePrefab, x * Vector3.right + ropeAttidude * Vector3.up, Quaternion.identity);
	}

}
