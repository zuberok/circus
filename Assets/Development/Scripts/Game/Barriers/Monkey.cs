﻿using UnityEngine;
using System.Collections;

public class Monkey : MonoBehaviour {

	public bool jump;
	Transform myTransform;
	float currentDelta;
	public float jumpingForce;
	public float jumpingSpeed;
	tk2dSpriteAnimator myAnim;
	public float walkingY;
	void Awake()
	{
		myAnim = GetComponent<tk2dSpriteAnimator>();
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("MonkeysTail"))
		{
			Jump();
		}
	}
	void Jump()
	{
		//if(renderer.isVisible)
		{
			transform.parent.rigidbody.AddForce(Vector3.up * jumpingForce + Vector3.left * jumpingSpeed);
			myAnim.Play("Jump");
		}
	}
	void Update()
	{
		if(transform.parent.rigidbody.velocity.y < 0)
		{
			myAnim.Play("Run");
		}
		if(transform.parent.localPosition.y < walkingY)
		{
			transform.parent.localPosition += (walkingY - transform.position.y) * Vector3.up * 1.5f;
		}
		if(jump)
		{
			Jump();
			jump = false;
		}
	}

}

