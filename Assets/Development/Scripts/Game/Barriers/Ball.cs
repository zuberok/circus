﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public enum Direction
	{
		Right,
		Left,
		Stay,
	}
	public static float maxX;
	public float startSpeed;
	public float velocity;
	public Direction startDirection;
	public Direction currentDirection;
	public float speed;
	Transform myTransform;
	bool entered;
	bool usable = false;
	void Awake()
	{
		entered = false;
		currentDirection = startDirection;
		speed = startSpeed;
		myTransform = transform.Find("Ball").transform;
	}
	void Liquidate()
	{
		Invoke("Remove", 2f);
	}
	void Remove()
	{
		if(transform.parent != null && transform.parent.childCount == 1)
		{
			Destroy(transform.parent.gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	public void Use()
	{
		speed *= velocity;
	}
	public void Drop()
	{
		speed /= velocity;
	}
	void Update()
	{
		switch (currentDirection)
		{
			case Direction.Right:
				transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.right, speed * Time.deltaTime);
				myTransform.localEulerAngles = Vector3.MoveTowards(myTransform.localEulerAngles, myTransform.localEulerAngles + Vector3.back, speed * 15f * Time.deltaTime);
				if(PlayerBalls.CurrentBall == this && maxX < transform.position.x)
				{
					maxX = transform.position.x;
				}
				break;
			case Direction.Left:
				transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.left, speed * Time.deltaTime);
				myTransform.localEulerAngles = Vector3.MoveTowards(myTransform.localEulerAngles, myTransform.localEulerAngles + Vector3.forward, speed * 15f * Time.deltaTime);
				break;
			default:
				break;
		}
	}
	void OnBecameVisible(){
		active = true;
	}
	void OnBecameInvisible(){
		if(active)
		{
			Destroy(gameObject);
		}
	}
	public void Activate(float x)
	{
		usable = true;
		maxX = transform.position.x;
		currentDirection = Direction.Stay;
		transform.position += (x - transform.position.x) * Vector3.right;
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Ball") && !entered && col.transform.position.x > transform.position.x)
		{
			if(PlayerBalls.alive && PlayerBalls.CurrentBall == this)
			{
				Sound.PlaySound("BallBump",2);
				PlayerBalls.Kill();
			}

			entered = true;
			currentDirection = Direction.Left;
			col.GetComponent<Ball>().currentDirection = Direction.Right;
		}
	}
}
