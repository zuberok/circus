﻿using UnityEngine;
using System.Collections;

public class ReplaceWhenInvisible : MonoBehaviour {
	public bool enterable;
	public float replacementDelta;
	public Renderer myRenderer;
	Transform myTransform;
	CreateCoinOnTheFloor gen;
	void Awake()
	{
		gen = GetComponent<CreateCoinOnTheFloor>();
		myTransform = transform;
	}
	void OnTriggerExit(Collider col)
	{
		if(col.CompareTag("Player") && col.isTrigger && col.GetComponent<Acrobat>() == null)
		{
			if(!enterable || (enterable && !myRenderer.isVisible))
			{
				//Debug.LogWarning(col.gameObject + " REPLACED FROM: " + myTransform.localPosition.x + " TO: " + (myTransform.localPosition.x + replacementDelta));
				Vector3 _temp = myTransform.localPosition;
				_temp.x += replacementDelta;
				myTransform.localPosition = _temp;
				if(gen != null)
				{
					gen.Clear();
					if(myTransform.position.x < LevelBuilder.Length - 20f)
					{
						gen.Generate();
					}
				}
			}
			
			
		}
		else if(col.GetComponent<Acrobat>() != null && col.GetComponent<Acrobat>().alive)
		{	
			//Debug.LogWarning(col.gameObject + " REPLACED FROM: " + myTransform.localPosition.x + " TO: " + (myTransform.localPosition.x + replacementDelta));
			myTransform.localPosition += replacementDelta * Vector3.right;
		}
	}
}
