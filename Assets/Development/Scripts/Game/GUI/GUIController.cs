﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {
	public int levelNumber;
	public GameObject pauseButton;
	public GameObject teachingPanel;
	static GameObject GameOverTextLabel;
	public GameObject gameOverTextLabel;
	public GameObject[] gameOverPanels;
	public GameObject[] winPanels;

	public static GUIController GUI;
	public UILabel loseText;
	public string losePhrase;
	public DisplayUIReward rewardDisplay;
	public UILabel gameOverScoreLabel;
	public UILabel scoreInfo;
	public UILabel winScoreLabel;
	static DisplayUIReward _rewardDisplay;
	public static bool NewHighScore = false;
	public GameObject newHighScoreLabel;
	public GameObject continuePlayMenu;
	public static LifesDisplay LifeDisplay;
	void Awake()
	{
		scoreInfo.text = GameController.Score.ToString();
		_rewardDisplay = rewardDisplay;
		GUI = this;
		GameOverTextLabel = gameOverTextLabel;
		if(!PlayerPrefs.HasKey("MANUAL_SHOWN"))
		{
			string _temp = string.Empty;
			for(int i = 1; i < levelNumber; ++i)
			{
				_temp += "0 ";
			}
			_temp += "1";
			for(int i = levelNumber; i < 5; ++i)
			{
				_temp += " 0";
			}
			PlayerPrefs.SetString("MANUAL_SHOWN", _temp);
			Teach();
		}
		else if (int.Parse(PlayerPrefs.GetString("MANUAL_SHOWN").Split()[levelNumber - 1]) == 0)
		{
			Teach();
			string[] _temp = PlayerPrefs.GetString("MANUAL_SHOWN").Split();
			_temp[levelNumber - 1] = "1";
			for(int i = 1; i < 5; ++i)
			{
				_temp[0] += " " + _temp[i];
			}
			PlayerPrefs.SetString("MANUAL_SHOWN", _temp[0]);
		}

	}
	public void Teach()
	{
		Time.timeScale = 0;
		teachingPanel.active = true;
	}

	public static void RecountLifes()
	{
		LifeDisplay.SetValue(GameController.Lifes);
	}

	public static void Reward(Transform t, int i)
	{
		_rewardDisplay.RewardDisplay(t, i);
	}

	public static void Win(int n)
	{
		foreach (GameObject panel in GUI.winPanels)
		{
			panel.active = true;			
		}
		GUI.winScoreLabel.text = n.ToString();
		Sound.Stop(4);
	}

	public static void Lose()
	{
		GUI.loseText.gameObject.active = true;
		GUI.loseText.text = GUI.losePhrase;
		GUI.Invoke("RestartLevel", 3f);
	}

	void RestartLevel()
	{
		GameController.LoadLevel(GameController.CurrentLevel);
	}

	public static void FinishScore(int n)
	{
		GUI.gameOverScoreLabel.text = n.ToString();
		if(n > Highscore.GetHighScoreWithNumber(1))
		{
			Debug.Log(GUI.newHighScoreLabel.active);
			GUI.newHighScoreLabel.active = true;
		}
	}
	
	void ShowMoneySpentInvite()
	{
		NGUITools.SetActive(continuePlayMenu, true);
		if(pauseButton != null){pauseButton.active = false;}
	}
	public void FinalGameOver()
	{
		Sound.PlayMusic("Pause", 0);
		foreach (GameObject panel in gameOverPanels)
		{
			NGUITools.SetActive(panel, true);
		}
		if(NewHighScore)
		{
			NewHighScore = false;
			newHighScoreLabel.active = true;
		}
		else
		{
			newHighScoreLabel.active = false;
		}
		Bonus.tempValue = -1;
		GUIController.FinishScore(int.Parse(GUIController.GUI.scoreInfo.text));
		Highscore.WriteResultIfInTop(GameController.TempScore + GameController.Score);
		PlayerPrefs.DeleteKey("PROGRESS");
	}
	public static void GameOver()
	{
		if (CoinTaker.count >= GameController.CoinsForResurrect)
		{
			Sound.PlayMusic("Pause", 0);
			Sound.Stop(4);
			GUI.ShowMoneySpentInvite();
		}
		else
		{
			Sound.PlayMusic("GameOver", 3);
			GUI.GameOverLabel();
		}
	}
	public void GameOverLabel()
	{
		GUI.loseText.gameObject.active = true;
		Sound.PlayMusic("GameOver", 0);
		Sound.Stop(4);
		GUI.loseText.text = "Game Over";
		GUI.Invoke("FinalGameOver", 3f);
	}
}
