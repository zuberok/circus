﻿using UnityEngine;
using System.Collections;

public class ShowOnClick : MonoBehaviour {

	bool showing = false;
	public float speedOfMoving;
	public Vector3 closePosition;
	public Vector3 openPosition;
	Transform myTransform;
	void Start()
	{
		myTransform = transform.parent;
		myTransform.localPosition = closePosition;
	}
	void OnClick()
	{
		showing = !showing;
	}
	void Update()
	{
		if((showing && myTransform.localPosition != openPosition) || (!showing && myTransform.localPosition != closePosition));
		{
			myTransform.localPosition = Vector3.MoveTowards(myTransform.localPosition, showing ? openPosition : closePosition, speedOfMoving * Time.deltaTime);
		}
	}
}
