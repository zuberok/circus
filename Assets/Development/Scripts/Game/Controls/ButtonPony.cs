﻿using UnityEngine;
using System.Collections;

public class ButtonPony : MonoBehaviour {

	public enum Direction
	{
		Left, Up,
	}
	public Rider player;
	public Direction direction;
	void OnPress(bool isPressed)
	{
		switch(direction)
		{
			case Direction.Left:				
				player.slow = isPressed;
				break;
			case Direction.Up:
				if(isPressed)
				{
					player.Jump(player.jumpingForce, false);
				}
				break;
		}

	}
}
