﻿using UnityEngine;
using System.Collections;

public class ButtonMonkey : MonoBehaviour {

	public enum Direction
	{
		Left, Right, Up,
	}
	public Direction direction;
	public PlayerMonkeys player;

	void OnPress(bool isPressed)
	{
		switch(direction)
		{
			case Direction.Left:				
				player.Left = isPressed;
				player.myAnim.Play(isPressed ? "Run" : "Idle");
				break;
			case Direction.Right:
				player.Right = isPressed;
				player.myAnim.Play(isPressed ? "Run" : "Idle");
				break;
			case Direction.Up:
				if(isPressed)
				{
					player.Jump();
				}
				break;
		}
		if(direction != Direction.Up && !isPressed && player.alive)
		{
			Sound.Stop(1);
		}
	}
}
