﻿using UnityEngine;
using System.Collections;

public class ButtonLion : MonoBehaviour {

	public enum Direction
	{
		Left, Right, Up,
	}
	public PlayerLion player;
	public Direction direction;
	
	bool left = false;
	bool right = false;
	void OnPress(bool isPressed)
	{
		switch(direction)
		{
			case Direction.Left:
				player.moveLeft = isPressed;
				if(!isPressed){ 
					Sound.Stop(1); 
					player.myAnim.Play("Idle");
				}
				break;
			case Direction.Right:
				player.moveRight = isPressed;
				if(!isPressed){ 
					Sound.Stop(1); 
					player.myAnim.Play("Idle");
				}
				break;
			case Direction.Up:
				if(isPressed)
				{
					player.Jump();
				}
				break;
		}

	}
}
