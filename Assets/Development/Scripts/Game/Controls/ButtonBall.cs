﻿using UnityEngine;
using System.Collections;

public class ButtonBall : MonoBehaviour {

	public enum Direction
	{
		Left, Right, Up,
	}
	public PlayerBalls player;
	public Direction direction;
	

	void OnPress(bool isPressed)
	{
		switch(direction)
		{
			case Direction.Left:
				if(player.currentBall != null && !player.auto)
				{				
					player.currentBall.GetComponent<Ball>().currentDirection = isPressed ? Ball.Direction.Left : Ball.Direction.Stay;
					if(!isPressed)
					{
						Sound.Stop(2);
					}
					else
					{
						Sound.PlaySound("BallRolling", 2);
					}
				}
				player.nextLeft = isPressed;					
				
				break;
			case Direction.Right:
				if(player.currentBall != null && !player.auto)
				{
					player.currentBall.GetComponent<Ball>().currentDirection = isPressed ? Ball.Direction.Right : Ball.Direction.Stay;
					if(!isPressed)
					{
						Sound.Stop(2);
					}
					else
					{
						Sound.PlaySound("BallRolling", 2);
					}
				}
				player.nextRight = isPressed;
				
				break;
			case Direction.Up:
				if(isPressed)
				{
					player.Jump();
				}
				break;
		}

	}
}
