﻿using UnityEngine;
using System.Collections;

public class SetPalelStatusOnEnable : MonoBehaviour {

	public GameObject panel;
	public bool status;
	void OnEnabled()
	{
		panel.active = status;
	}
}
