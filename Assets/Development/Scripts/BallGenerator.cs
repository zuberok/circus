﻿using UnityEngine;
using System.Collections;

public class BallGenerator : MonoBehaviour {

	public float pivotDistance;
	public float timer;
	public float maxDistanceFromPlayer;
	public GameObject[] balls;
	public float ballHeight;
	Transform previousBall;


	private Vector3 ballInstVector;
	void Start()
	{
		InitBall();
	}
	void InitBall()
	{
		ballInstVector = previousBall == null ? (transform.position.x + 55f) * Vector3.right + ballHeight * Vector3.up : (previousBall.position.x + pivotDistance) * Vector3.right + ballHeight * Vector3.up;

		if(ballInstVector.x < GetComponent<LevelBuilder>().levelLength * 5 - 80f)
		{
			if(ballInstVector.x - transform.position.x <= maxDistanceFromPlayer)
			{
				previousBall = ((GameObject)Instantiate(balls[Random.Range(0, balls.Length)], ballInstVector, Quaternion.identity)).transform;
				foreach(Ball ball in gameObject.GetComponentsInChildren<Ball>())
				{
					ball.currentDirection = Ball.Direction.Left;
				}
			}
			Invoke("InitBall", timer);
		}
		else
		{
			Debug.Log(ballInstVector.x);
		}
	}
	
}
