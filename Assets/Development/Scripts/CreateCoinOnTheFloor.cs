﻿using UnityEngine;
using System.Collections.Generic;

public class CreateCoinOnTheFloor : MonoBehaviour {
	public GameObject[] coins;
	public Vector3[] coinCoordinates;
	List<GameObject> currentCoins;
	void Awake()
	{
		currentCoins = new List<GameObject>();
		Generate();
	}
	public void Generate()
	{
		Transform _temp;
		for(int i = 0; i < coinCoordinates.Length; ++i)
		{
			_temp = ((GameObject)Instantiate(coins[Random.Range(0, coins.Length)])).transform;
			_temp.parent = transform;
			_temp.localPosition = coinCoordinates[i];
			if(_temp.position.x < LevelBuilder.Length)
			{
				_temp.parent = null;
				currentCoins.Add(_temp.gameObject);
			}
			else
			{
				Destroy(_temp.gameObject);
				break;
			}
		}
	}
	public void Clear()
	{
		foreach (GameObject go in currentCoins)
		{
			Destroy(go);
		}
		currentCoins.Clear();
	}
}
