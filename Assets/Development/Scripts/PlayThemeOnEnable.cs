﻿using UnityEngine;
using System.Collections;

public class PlayThemeOnEnable : MonoBehaviour {
	public int source;
	public string enableThemeName;
	public string disableThemeName;
	void OnEnable()
	{
		if(enableThemeName != string.Empty)Sound.PlayMusic(enableThemeName, source);
	}
	void OnDisable()
	{
		if(disableThemeName != string.Empty)Sound.PlayMusic(disableThemeName, source);
	}
}
