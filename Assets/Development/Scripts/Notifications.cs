﻿using UnityEngine;
using System.Collections;

public class Notifications : MonoBehaviour {

	static bool Runned = false;

	void Start()
	{
		if (Runned = false)
		{
			LocalNotificationLibrary.destroy();
			DontDestroyOnLoad (gameObject);
			Runned = true;
		} else 
		{
			Destroy(gameObject);
		}
	}
	
	void OnApplicationPause(bool ispause)
	{
		
		if (ispause)
		{
			NotificationService ();
		}
		
		if (!ispause)
		{
			LocalNotificationLibrary.destroy();
		}
	}
	
	void NotificationService() {
		LocalNotificationLibrary.create(0, 0, 20,
		                                "Circus", "My Friend!", "Time to Play");
	}
}
