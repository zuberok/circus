﻿using UnityEngine;
using System.Collections;

public class MonkeyScore : MonoBehaviour {

	public int count;
	//public GameObject countDisplay;
	public Vector3 displayPosition;
	void OnTriggerExit(Collider col)
	{
		if(col.CompareTag("Player") && col.GetComponent<PlayerMonkeys>().alive && col.isTrigger)
		{
			GameController.TempScore += count;
			GUIController.Reward(transform, count);
		}
	}
}
