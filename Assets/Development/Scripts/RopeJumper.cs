﻿using UnityEngine;
using System.Collections;

public class RopeJumper : MonoBehaviour {

	public Vector3 vector;
	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Player"))
		{
			col.rigidbody.velocity = Vector3.zero;
			col.GetComponent<Acrobat>().Jump(vector);
		}
		
	}
}
