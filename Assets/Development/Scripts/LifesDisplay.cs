﻿using UnityEngine;
using System.Collections;

public class LifesDisplay : MonoBehaviour {

	static GameObject[] images;
	void OnEnable()
	{
		images = new GameObject[] {transform.Find("2_1").gameObject, transform.Find("2_2").gameObject, transform.Find("2_3").gameObject};
		GUIController.LifeDisplay = this;
		GUIController.RecountLifes();
	}
	public void SetValue(int val)
	{
		for(int i = 3; i > val; --i)
		{
			images[i - 1].active = false;
		}
	}
}
