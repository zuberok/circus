﻿using UnityEngine;
using System.Collections;

public class CoinTaker : MonoBehaviour {

	public UILabel text;
	static CoinTaker ct;
	public static int count
	{
		get
		{
			return PlayerPrefs.GetInt("COINS");
		}
		set
		{
			if(ct.text != null) {ct.text.text = value.ToString();}
			PlayerPrefs.SetInt("COINS", value);
		}
	}
	void Awake()
	{
		ct = this;
		count = PlayerPrefs.GetInt("COINS");
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Coin"))
		{
			Destroy(col.gameObject);
			count++;
			Sound.PlaySound("CoinTake", 1);
		}
	}
}
