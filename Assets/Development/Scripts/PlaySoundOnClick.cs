﻿using UnityEngine;
using System.Collections;

public class PlaySoundOnClick : MonoBehaviour {
	public int source;
	public string soundName;
	void OnClick()
	{
		Sound.PlaySound(soundName, source);
	}
}
