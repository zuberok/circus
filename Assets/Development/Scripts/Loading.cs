﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour {

	public static int LevelToLoad = 1;
	static Loading l;
	void Awake()
	{
		l = this;
	}
	static void Load() 
	{
        Time.timeScale = 1;
		l.StartCoroutine("LoadCoroutine");
		Debug.LogWarning(LevelToLoad);
	}
	
	IEnumerator LoadCoroutine()
	{
		yield return new WaitForSeconds(0.1f);
		Application.LoadLevelAsync(LevelToLoad);        
	}

	void OnApplicationQuit()
	{
		LevelToLoad = 1;
	}
}
