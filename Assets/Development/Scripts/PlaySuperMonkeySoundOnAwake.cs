﻿using UnityEngine;
using System.Collections;

public class PlaySuperMonkeySoundOnAwake : MonoBehaviour {

	void OnBecameVisible() 
	{
		Sound.PlaySound("SuperMonkey",2);
	}
}
