﻿using UnityEngine;
using System.Collections.Generic;

public class Sound : MonoBehaviour {

	public static Sound Controller;

	public AudioSource[] sources;

	public Dictionary<string, AudioClip> themes;
	public Dictionary<string, AudioClip> sounds;

	//Music Themes
	public AudioClip mainMenu;
	public AudioClip gameProcess;
	public AudioClip loadingPause;
	public AudioClip lose;
	public AudioClip win;
	public AudioClip gameOver;

	//Common Sounds
	public AudioClip coinTake;

	//Monkey Level
	public AudioClip superMonkey;
	public AudioClip monkeyPlayerWalk;
	public AudioClip monkeyPlayerJump;

	//Pony Level
	public AudioClip horseWalking;
	public AudioClip riderHorseJump;
	public AudioClip riderJumperJump;

	//Lion Level
	public AudioClip lionWalking;
	public AudioClip lionJump;
	public AudioClip lionBurn;
	public AudioClip gobletFire;

	//Balls Level
	public AudioClip ballRolling;
	public AudioClip ballJump;
	public AudioClip ballBump;

	//Ropes Level
	public AudioClip trampolineJump;
	public AudioClip ropeJump;

	void Awake()
	{
		if(Controller != null)
		{
			Destroy(Controller.gameObject);
		}
		Controller = this;
		sounds = new Dictionary<string, AudioClip>()
		{			
		  	{"CoinTake", coinTake},

			{"SuperMonkey", superMonkey},
		  	{"MonkeyPlayerWalk", monkeyPlayerWalk},
		  	{"MonkeyPlayerJump", monkeyPlayerJump},	
		  	
		  	{"HorseWalking", horseWalking},
		  	{"RiderHorseJump", riderHorseJump},
		  	{"RiderJumperJump", riderJumperJump},
		
		  	{"LionWalking", lionWalking},
		  	{"LionJump", lionJump},
		  	{"LionBurn", lionBurn},
		  	{"GobletFire", gobletFire},

		  	{"BallRolling", ballRolling},
		  	{"BallJump", ballJump},
		  	{"BallBump", ballBump},

		  	{"TrampolineJump", trampolineJump},
		  	{"RopeJump", ropeJump},
		};
		themes = new Dictionary<string, AudioClip>()
		{	
			{"GameProcess", gameProcess},	
			{"MainMenu", mainMenu},
	  		{"Pause", loadingPause},
			{"Lose", lose},
			{"Win", win},
			{"GameOver", gameOver},	
		};
		DontDestroyOnLoad(gameObject);
		//PlayMusic("Main", 0);
	}

	
	public static void PlayMusic(string name, int source)
	{
		// foreach (AudioSource s in Controller.sources)
		// {
		// 	s.Stop();
		// }
		Controller.sources[source].clip = Controller.themes[name];
		Controller.sources[source].Play();
		Debug.Log(name + " is playing on "+ source);
	}
	public static void PlaySound(string name, int source)
	{
		Controller.sources[source].clip = Controller.sounds[name];
		Controller.sources[source].Play();
		Debug.Log(name + " is playing on "+ source);
		//Debug.Log(name + " IS PLAYING AT SOURCE " + source);
	}
	public static void Stop(int n)
	{
		if(Controller.sources[n] != null ) 
		{
			Controller.sources[n].Stop();
			Debug.LogWarning("SOUND STOPPED AT SOURCE " + n);
		}

	}
	public static bool IsPlaying(int i, string soundName)
	{
		return (Controller.sources[i].clip == Controller.sounds[soundName] && Controller.sources[1].isPlaying);
	}
}
