﻿using UnityEngine;
using System.Collections;

public class HighscoreLabel : MonoBehaviour {

	void OnEnable()
	{
		GetComponent<UILabel>().text = Highscore.GetHighScoreWithNumber(1).ToString();
	}
}
