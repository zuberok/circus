﻿using UnityEngine;
using System.Collections;

public class FireCup : MonoBehaviour {
	bool shown = false;
	void OnBecameVisible(){
		shown = true;
		Sound.PlaySound("GobletFire", 2);
	}
	void OnBecameInisible(){
		if(shown)
		{
			Sound.Stop(2);
			Destroy(transform.parent.gameObject);
		}
		
	}
}
