﻿using UnityEngine;
using System.Collections;

public class LocalNotificationLibrary : MonoBehaviour {
	public static void create (int seconds, int minutes, int hours,
									string tickerTitle, string contentTitle, string notificationContent) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaObject libObj = null;
		AndroidJavaObject playerActivityContext = null;
		
		using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
			playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
		
		using (var pluginClass = new AndroidJavaClass("org.disponet.localNotificationLibrary.AlarmService")) {
			if (pluginClass != null) {
				libObj = pluginClass.CallStatic<AndroidJavaObject>("getInstance");
				libObj.Call("alarmServiceStart", playerActivityContext,
							tickerTitle, contentTitle, notificationContent,
							seconds, minutes, hours);
			}
		}
		#endif
	}

	public static void destroy () {
		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaObject libObj = null;
		AndroidJavaObject playerActivityContext = null;
		
		using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
			playerActivityContext = actClass.GetStatic<AndroidJavaObject>("currentActivity");
		}
		
		using (var pluginClass = new AndroidJavaClass("org.disponet.localNotificationLibrary.AlarmService")) {
			if (pluginClass != null) {
				libObj = pluginClass.CallStatic<AndroidJavaObject>("getInstance");
				libObj.Call("alarmServiceStop", playerActivityContext);
			}
		}
		#endif
	}
}
