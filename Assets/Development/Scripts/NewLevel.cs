﻿using UnityEngine;
using System.Collections;

public class NewLevel : MonoBehaviour {
	public float delay;
	public int levelNumber;
	void OnClick()
	{
		Invoke("Use", delay);
	}
	void Use()
	{
		GameController.LoadScore();
		GameController.LoadLevel(levelNumber);
	}
}
