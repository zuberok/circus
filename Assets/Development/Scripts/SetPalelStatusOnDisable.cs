﻿using UnityEngine;
using System.Collections;

public class SetPanelStatusOnEnable : MonoBehaviour {

	public GameObject panel;
	public bool status;
	void OnEnable()
	{
		panel.active = status;
	}
}
