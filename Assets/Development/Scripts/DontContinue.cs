﻿using UnityEngine;
using System.Collections;

public class DontContinue : MonoBehaviour {

	// Use this for initialization
	void OnClick()
	{
		Bonus.tempValue = -1;
		GUIController.GUI.GameOverLabel();
	}
}
