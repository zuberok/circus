﻿using UnityEngine;
using System.Collections;

public class RingGenerator : MonoBehaviour 
{
	public float pivotDistance;
	public float timer;
	public GameObject ring;
	public float ringHeight;
	Transform previousRing;

	public GameObject fireCup;
	public float fireCupStep;
	public float fireCupHeight;


	private Vector3 ringInstVector;
	void Start()
	{
		InitRing();
		for(int i = 1; i * fireCupStep < GetComponent<LevelBuilder>().levelLength * 5; ++i)
		{
			Instantiate(fireCup, i * fireCupStep * Vector3.right + fireCupHeight * Vector3.up, Quaternion.identity);
		}
	}
	void InitRing()
	{
		ringInstVector = previousRing == null ? (transform.position.x + 55f) * Vector3.right + ringHeight * Vector3.up : (previousRing.position.x + pivotDistance) * Vector3.right + ringHeight * Vector3.up;

		if(ringInstVector.x < GetComponent<LevelBuilder>().levelLength * 5)
		{
			previousRing = ((GameObject)Instantiate(ring, ringInstVector, Quaternion.identity)).transform;
			Invoke("InitRing", timer);
		}
		else
		{
			Debug.Log(ringInstVector.x);
		}
	}
}