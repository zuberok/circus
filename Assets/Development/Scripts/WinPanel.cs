﻿using UnityEngine;
using System.Collections;

public class WinPanel : MonoBehaviour {

	public UILabel infoText;
	public string wintext;
	void OnEnable()
	{
		Invoke("OpenPanel", 4f);
		infoText.gameObject.active = true;
		infoText.text = wintext;	}
	void OpenPanel()
	{
		transform.Find("Menu").active = true;
		Sound.PlayMusic("Pause", 3);
		infoText.gameObject.active = false;
	}
}
