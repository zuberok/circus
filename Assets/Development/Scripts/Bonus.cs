﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {

	public static Bonus bonus;
	public int startScore;
	public int decreasingStep;
	public float decreasingPeriod;
	public UILabel text;
	int score;
	bool decreasing = true;
	public static int tempValue = -1;
	public int currentScore
	{
		set
		{
			if (text != null)	{text.text = value.ToString(); }
			score = value;
		}
		get 
		{
			return score;
		}
	}
	public static int lastValue
	{
		get { return bonus.currentScore;}

	}
	public void Disable()
	{
		decreasing = false;
	}
	void Start () 
	{  
		bonus = this;
		if(tempValue != -1)
		{
			currentScore = tempValue;
			Debug.Log(tempValue);
		}
		else
		{
			currentScore = startScore;
		}
		Invoke("DecreaseScore", decreasingPeriod);
	}
	void DecreaseScore()
	{
		if(decreasing)
		{
			currentScore = Mathf.Clamp(currentScore - decreasingStep, 0, startScore);
			Invoke("DecreaseScore", decreasingPeriod);
		}
	}
}
