﻿using UnityEngine;
using System.Collections;

public class GoToMain : MonoBehaviour {

	void OnClick()
	{
		GameController.TempScore = 0;
		Loading.LevelToLoad = 1;
		Application.LoadLevel(1);
	}
}
