﻿using UnityEngine;
using System.Collections;

public class CreateCoinInside : MonoBehaviour {

	public int chanceInPercents; // 0.25
	public Vector3 insidePosition;
	public GameObject coin;
	void Start()
	{
		int i = Random.Range(1, 101);
		if(i < chanceInPercents && transform.position.x + insidePosition.x < LevelBuilder.Length - 50f)
		{
			Generate();
		}

	}
	void Generate()
	{

		Transform _temp = ((GameObject)Instantiate(coin)).transform;
		_temp.parent = transform;
		_temp.localPosition = insidePosition;
		if(GetComponents<Jumper>() != null)
		{
			_temp.transform.parent = null;
		}
	}
}
