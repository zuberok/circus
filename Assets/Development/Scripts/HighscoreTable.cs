﻿using UnityEngine;
using System.Collections;

public class HighscoreTable : MonoBehaviour {

	void OnEnable()
	{
		for(int i = 1; i < 5; ++i)
		{
			transform.Find(i.ToString() + "/Score").GetComponent<UILabel>().text = Highscore.GetHighScoreWithNumber(i).ToString();
		}
	}
}
