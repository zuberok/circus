﻿using UnityEngine;
using System.Collections;

public class DestroyOnUse : MonoBehaviour {

	void OnTriggerExit(Collider col){
		if(col.CompareTag("Player"))
		{
			Destroy(gameObject);
		}
	}
}
