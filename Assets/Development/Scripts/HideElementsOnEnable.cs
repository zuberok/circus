﻿using UnityEngine;
using System.Collections;

public class HideElementsOnEnable : MonoBehaviour {
	public GameObject[] objects;
	void OnEnable()
	{
		foreach (GameObject go in objects)
		{
			go.active = false;
		}
	}
}
