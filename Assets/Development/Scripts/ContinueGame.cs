﻿using UnityEngine;
using System.Collections;

public class ContinueGame : MonoBehaviour {

	void OnClick()
	{
		GameController.Lifes++;
		CoinTaker.count -= GameController.CoinsForResurrect;
		GameController.TempScore = 0;
		Bonus.tempValue = Bonus.bonus.currentScore;
		GameController.LoadLevel(GameController.CurrentLevel);
	}
}
