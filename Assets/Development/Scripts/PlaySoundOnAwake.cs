﻿using UnityEngine;
using System.Collections.Generic;

public class PlaySoundOnAwake : MonoBehaviour {
	public int source; // порядковый номер динамика для проигрывания звука
	public string soundName; //названия звука, который нужно проиграть
	public enum SoundType //типы проигрываемых звуков. Определяется для каждого объекта отдельно
	{
		Music, Sound,
	}
	public SoundType soundType; //тип проигрываемого звука для этого объекта
	
	// эtакий тип метода. Хочешь ты проиграть звук или музыку - всё равно будешь использовать этот тип
	delegate void PlaySoundDelegate(string soundName, int source); 
	
	static Dictionary<SoundType, PlaySoundDelegate> soundPlayMethods = new Dictionary<SoundType, PlaySoundDelegate>()
	{
		{SoundType.Sound, Sound.PlaySound},
		{SoundType.Music, Sound.PlayMusic}, // в зависимости от soundType будет вызываться соответствующий метод в Start()
	};
	void Start()
	{
		soundPlayMethods[soundType](soundName, source);
		for (int i = 0; i < Sound.Controller.sources.Length; ++i)
		{
			if(i == source) continue;
			Sound.Stop(i);
		}
	}
}
