﻿using UnityEngine;
using System.Collections;

public class PauseSoundOnEnable : MonoBehaviour {

	public string name;
	public int source;

	void OnEnable()
	{
		Sound.Controller.sources[source].Pause();
	}
	void OnDisable()
	{
		Sound.PlaySound(name, source);
	}
}
